# ebuildbot

_Automated ebuild maintenance._

## License

SPDX-License-Identifier: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

## Reference

- [Alpine Linux - aports travis scripts](https://git.alpinelinux.org/aports/tree/.travis)

- [Gentoo - scripts to run mirroring and continuous integration](https://github.com/mgorny/repo-mirror-ci)

## See Also

- [ebuildtester](https://github.com/nicolasbock/ebuildtester)

- [GKernelCI](https://github.com/gentoo/Gentoo_kernelCI)
