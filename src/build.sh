#!/bin/sh
# SPDX-License-Identifier: GPL-3.0-only
set -eu

fetch_changed() {

  git diff-tree -r --name-only \
                   --diff-filter=ACMR HEAD \
                   -- '*ebuild' | awk '/ebuild/ { print }'
}

parse_category() {

  EBUILD="${1}"
  
  echo "${EBUILD}" | awk '{ split($0, a, "/"); print a[1] }'

}

parse_name() {

  EBUILD="${1}"
  
  echo "${EBUILD}" | awk '{ split($0, a, "/"); print a[2] }'

}

build_package() {

  EBUILD="${1}"
  CATEGORY=$(parse_category "${EBUILD}")
  P=$(parse_name "${EBUILD}")
  ATOM="${CATEGORY}/${P}"

  emerge --keep-going=y "${ATOM}"
}

main() {

  EBUILDS=$(fetch_changed)

  for P in $EBUILDS; do
    build_package "${P}"
  done
}

main
